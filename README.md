### CREDIT values

Example on scanning issues and charting based on label counts.
One could make an `issue` anytime a win or accomplishment occurred
and add labels with corresponding [GitLab values](https://about.gitlab.com/handbook/values/).


![plot](./plot.png)
